import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';

import { SwingModule } from 'angular2-swing'

import { HttpClientModule } from '@angular/common/http';
import { CardsComponent } from './cards/cards.component'

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    CardsComponent
  ],
  imports: [
    BrowserModule,
    SwingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
