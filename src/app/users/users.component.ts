import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private http:HttpClient) { }

  users

  fetchUsers(){
    this.http.get('http://localhost:3000/users/')
      .subscribe(users => this.users = users)
  }

  ngOnInit() {
    this.fetchUsers()
  }

}
